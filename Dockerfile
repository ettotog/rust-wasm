FROM rust:latest
RUN cargo install -f wasm-bindgen-cli
RUN rustup target add wasm32-unknown-unknown
RUN apt update && apt install -y \
    gettext \
    clang \
    && rm -rf /var/lib/apt/lists/*
COPY entrypoint.sh /root/entrypoint.sh
COPY templates/ /root/templates
COPY assets/ /root/assets
ENV TITLE="Bevy Game"
ENTRYPOINT ["/root/entrypoint.sh"]
