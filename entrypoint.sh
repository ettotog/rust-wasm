#!/bin/bash
set -e

cargo build --release --target wasm32-unknown-unknown
wasm-bindgen --out-dir ./public --target web $(ls target/wasm32-unknown-unknown/release/*.wasm)
export WASM=$(basename $(ls public/*.wasm))
export JS=${WASM%_bg.wasm}.js
envsubst < /root/templates/${TEMPLATE_NAME:-main}.html > ./public/index.html
cp -r /root/assets ./public/assets
cp -r assets/* ./public/assets
